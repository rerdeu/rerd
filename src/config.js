module.exports.SITE = {
  name: 'Re: Research & Development',

  origin: 'https://rerd.eu',
  basePathname: '/',
  trailingSlash: false,

  title: 'R&D 🚀',
  description: 'Rethink Remap Remake Remix Restore Recharge Renew your research & development with innovation and passion.',
};
